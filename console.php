<?php
declare(strict_types = 1);
require_once(__DIR__.'/vendor/autoload.php');

use \Kix\IO\{InputOptions, LogFile, CsvFile};

set_error_handler(static function ($errno, $errStr) {
    throw new \Kix\Exception\ErrorException($errStr, $errno);
});

function plus(array $input): int {
    return $input[0] + $input[1];
}

function minus(array $input): int {
    return $input[0] - $input[1];
}

function division(array $input): float {
    return $input[0] / $input[1];
}

function multiply(array $input): int {
    return $input[0] * $input[1];
}

$options = InputOptions::createFromGlobals();
$logFile = new LogFile();
$inputFile = new CsvFile($options->getFile());
$outputFile = new \SplFileObject('result.csv', 'a+');

$iterator = $inputFile->iterate();
$action = $options->getAction();
$cast = static function($val) {
    return (int) $val;
};

$logFile->write('Started '.$action.' operation');

while ($line = $iterator->current()) {
    $iterator->next();

    try {
        $result = $action(array_map($cast, $line));
    } catch (\Kix\Exception\ErrorException $e) {
        $result = 0;
    }

    if ($result <= 0) {
        $logFile->write('numbers '. implode(' and ', $line) . ' are wrong');
        continue;
    }

    $line []= $result;
    $outputFile->fwrite(implode(';', $line + [$result]). "\r\n");
}

$logFile->write('Finished '.$action.' operation');
<?php
declare(strict_types=1);

namespace Kix\IO;

final class CsvFile
{
    private $file;

    private $delimiter;

    public function __construct(string $filename, string $delimiter = ';')
    {
        $this->file = new \SplFileObject($filename);
        $this->delimiter = $delimiter;
    }

    public function iterate(): \Generator
    {
        while ($line = $this->file->fgetcsv($this->delimiter)) {
            yield $line;
        }
    }
}
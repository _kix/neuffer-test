<?php
declare(strict_types=1);

namespace Kix\IO;


final class LogFile
{
    private $file;

    public function __construct(string $filename = 'log.txt')
    {
        if (file_exists($filename)) {
            unlink($filename);
        }

        $this->file = new \SplFileObject($filename, 'a+');
    }

    public function write(string $message): void
    {
        $this->file->fwrite($message." \r\n");
    }
}
<?php
declare(strict_types=1);

namespace Kix\IO;


final class InputOptions
{
    public const ACTION_PLUS = 'plus';

    public const ACTION_MINUS = 'minus';

    public const ACTION_DIVISION = 'division';

    public const ACTION_MULTIPLY = 'multiply';

    public const SUPPORTED_ACTIONS = ['plus', 'minus', 'division', 'multiply'];

    private const DEFAULT_ACTION = 'xyz';

    private const DEFAULT_FILE = 'notexists.csv';

    private const SHORT_OPTIONS = 'a:f:';

    private const LONG_OPTIONS = [
        'action:',
        'file:',
    ];


    private $action;

    private $file;

    private function __construct(array $options)
    {
        if (array_key_exists('a', $options)) {
            $this->action = $options['a'];
        } elseif (array_key_exists('action', $options)) {
            $this->action = $options['action'];
        } else {
            $this->action = self::DEFAULT_ACTION;
        }

        if (!in_array($this->action, self::SUPPORTED_ACTIONS, true)) {
            throw new \InvalidArgumentException(sprintf(
                'Action %s is not supported. Use one of: %s',
                $this->action,
                implode(', ', self::SUPPORTED_ACTIONS)
            ));
        }

        if (array_key_exists('f', $options)) {
            $this->file = $options['f'];
        } elseif (array_key_exists('file', $options)) {
            $this->file = $options['file'];
        } else {
            $this->file = self::DEFAULT_FILE;
        }
    }

    public static function createFromGlobals(): self
    {
        return new self(
            getopt(self::SHORT_OPTIONS, self::LONG_OPTIONS)
        );
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getFile(): string
    {
        return $this->file;
    }
}
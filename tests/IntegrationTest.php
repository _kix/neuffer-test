<?php
declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;

final class IntegrationTest extends TestCase
{
    protected function setUp(): void
    {
        unlink(__DIR__.'/../result.csv');
        unlink(__DIR__.'/../log.txt');
    }

    public function provideActions(): array
    {
        return [
            ['plus'],
            ['minus'],
            ['multiply'],
            ['division'],
        ];
    }

    /**
     * @test
     * @group functional
     * @dataProvider provideActions
     */
    public function it_yields_correct_output(string $action): void
    {
        exec('php console.php --action '.$action.' --file test.csv');

        static::assertFileEquals(__DIR__.'/fixtures/'.$action.'.csv', __DIR__.'/../result.csv');
        static::assertFileEquals(__DIR__.'/fixtures/'.$action.'.log', __DIR__.'/../log.txt');
    }
}
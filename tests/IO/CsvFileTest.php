<?php
declare(strict_types=1);

namespace Tests\IO;

use Kix\IO\CsvFile;
use PHPUnit\Framework\TestCase;

class CsvFileTest extends TestCase
{
    /**
     * @test
     */
    public function it_is_initializable(): void
    {
        $csvFile = new CsvFile(__DIR__.'/../../test.csv');

        static::assertInstanceOf(CsvFile::class, $csvFile);
    }

    /**
     * @test
     */
    public function it_yields_values(): void
    {
        $csvFile = new CsvFile(__DIR__.'/../../test.csv');

        foreach ($csvFile->iterate() as $row) {
            static::assertIsArray($row);
        }
    }
}